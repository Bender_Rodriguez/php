<?php

$DB_DSN = 'mysql:host=localhost;dbname=accounts';
$DB_USERNAME = "root";
$DB_PASSWORD = "";
$DB_OPTIONS = array(
	PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
	PDO::ERRMODE_EXCEPTION
);

$db = new PDO($DB_DSN, $DB_USERNAME, $DB_PASSWORD, $DB_OPTIONS);

if (!$db->query('CREATE DATABASE accounts'))
	printf("Error: %s\n", $db->error);

$db->query('
CREATE TABLE `accounts`.`users`
(`id` INT NOT NULL AUTO_INCREMENT,
`username` VARCHAR(50) NOT NULL,
`email` VARCHAR(100) NOT NULL,
`hash` VARCHAR(32) NOT NULL,
`active` BOOL NOT NULL DEFAULT 0,
PRIMARY KEY (`id`)
);') or die($db->error);

?>
