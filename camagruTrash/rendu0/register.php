<?php
/* Registration process, inserts user info into the database 
   and sends account confirmation email message
 */

//Set session variable used on profile.php page
$_SESSION['email'] = $_POST['email'];
$_SESSION['username'] = $_POST['username'];


//Escape all $_POST variables to protect against SQL injections
$username = $dbh->escape_string($_POST['username']);
$email = $dbh->escape_string($_POST['email']);
$password = $dbh->escape_string(password_hash($_POST['password'], PASSWORD_BCRYPT));
$hash = $dbh->escape_string(md5(rand(0, 1000)));



//Check if user with that email already exists
$result = $dbh->query("SELECT * FROM users WHERE email='$email'") or die($dbh->error());
//If there is at leat 1 row, the user email already exists
if ($result->num_rows > 0)
{
	$_SESSION['message'] = 'User with this email already exists';
	header("location:error.php");
}


?>